# Logger

A general purpose logging library utilizing zerolog.

## Installation

```shell
go get -u gitlab.com/autosigma/utilities/logger
```
