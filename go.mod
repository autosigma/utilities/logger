module gitlab.com/autosigma/utilities/logger

go 1.17

require (
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/zerolog v1.26.1
)
