package log

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"
	"io"
	"os"
	"strings"
)

var _ io.Writer = (*AutoSigmaDefaultWriter)(nil)
var _ io.Writer = (*AutoSigmaDefaultErrorWriter)(nil)

var Logger = NewDefaultLogger()

func NewDefaultLogger() zerolog.Logger {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	setGlobalLogLevel(os.Getenv("AUTOSIGMA_DEBUG_LEVEL"))

	if strings.ToLower(os.Getenv("AUTOSIGMA_LOGGER")) == "console" {
		return zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()
	} else {
		return zerolog.New(os.Stdout).With().Timestamp().Logger()
	}
}

func EnableTrace() {
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	Logger = Logger.With().Stack().Logger()
}

func SetGlobalLogLevel(level string) {
	if !setGlobalLogLevel(level) {
		Logger.Info().Msg("the error level string did not match an error level")
	}
}

func setGlobalLogLevel(level string) bool {
	if lvl, err := zerolog.ParseLevel(level); err != nil || lvl == zerolog.NoLevel {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
		return false
	} else {
		zerolog.SetGlobalLevel(lvl)
		return true
	}
}

func EnableConsole() {
	Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger()
}

func Err(err error) *zerolog.Event {
	return Logger.Err(err)
}

func Trace() *zerolog.Event {
	return Logger.Trace()
}

func Debug() *zerolog.Event {
	return Logger.Debug()
}

func Info() *zerolog.Event {
	return Logger.Info()
}

func Warn() *zerolog.Event {
	return Logger.Warn()
}

func Error() *zerolog.Event {
	return Logger.Error()
}

func Fatal() *zerolog.Event {
	return Logger.Fatal()
}

func Panic() *zerolog.Event {
	return Logger.Panic()
}

func With() zerolog.Context {
	return Logger.With()
}

type AutoSigmaDefaultWriter struct{}
type AutoSigmaDefaultErrorWriter struct{}

func (a AutoSigmaDefaultWriter) Write(p []byte) (n int, err error) {
	Logger.Info().Msg(string(p))
	return len(p), nil
}

func (a AutoSigmaDefaultErrorWriter) Write(p []byte) (n int, err error) {
	Logger.Error().Msg(string(p))
	return len(p), nil
}
